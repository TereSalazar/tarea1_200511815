# TABLA DE CONTENIDO
* Información general
* Tecnología
* Instalación

## Información general
* Utilizar api.softwareavanzado.world para desarrollar un cliente de webservice. [enlace](https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal&format=doc)
* Documentación técnica de las credenciales: [enlace](http://redcomponent-com.github.io/redCORE/?chapters/oauth2/grant_type_client_credentials.md)
* Autenticar usando client credentials.
* Client ID = correo según DTT.  Client secret = carné.
* Listar contactos.
* Crear 10 contactos incluyendo su número de carnet en el nombre.

## Tecnología
* Node js version 12.14.1

## Instalación
* Descargar o clonar proyecto
* Dirigirse en consola a la carpeta donde se descargó o clonó el proyecto
* Escribir en consola node index.js para ejecutar el proyecto