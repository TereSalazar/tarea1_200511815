const cliente = require('request');

getToken();


//FUNCION PARA OBTENER CLIENTS CREDENCIALS
function getToken(){
    let obj= {
        "grant_type": 'client_credentials',
        "client_id": 'zandratere@gmail.com',
        "client_secret": '200511815'
    };

    let options = {
        url: 'https://api.softwareavanzado.world/index.php?option=token&api=oauth2',
        method: 'POST',
        json: obj 
    };

    cliente(options, (error, response, body)=>{
        if(!error && response.statusCode == 200){
            console.log(body.access_token);
            crearContacto(body.access_token);
            listarContacto(body.access_token);
        }else{
            console.log(response);
        }
    })
}


//FUNCION PARA CREAR CONTACTOS
function crearContacto(numToken){

        for(let num=0; num <10;num++){
            let obj={
                "name" : "200511815_Tere" + num,
                "catid" : "1",
            };
        
            let options = {
                url:'https://api.softwareavanzado.world/index.php?' +
                    'webserviceClient=administrator&'+
                    'webserviceVersion=1.0.0&' +
                    'option=contact&'+
                    'api=hal&' +
                    'access_token=' + numToken,
                method: 'POST',
                json: obj
        };

        cliente(options, (error, response, body)=>{
            if(!error && response.statusCode == 201){
                console.log("Insertado con exito");
            }else{
                console.log("Error al insertar");
            }
        });
    }
}


//FUNCION PARA LISTAR CONTACTO
function listarContacto(numToken){
    let options = {
        url: 'https://api.softwareavanzado.world/index.php?' +
            'webserviceClient=administrator&'+
            'webserviceVersion=1.0.0&' +
            'option=contact&'+
            'api=hal&' +
            'access_token=' + numToken+
            '&list[limit]=0',
        method: 'GET',
        json: true
    };

    cliente(options,(error,response,body)=>{
        if(!error && response.statusCode == 200){
            console.log("*****************LISTADO CONTACTOS************");
            console.log(body._embedded.item);
        }else{
            console.log("Error el mostrar listado");
        }
    })
}

